var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Grid = require('gridfs-stream');
var multipart = require('connect-multiparty');
var fs = require('fs');
var owasp = require('owasp-password-strength-test');

//Database related variables (used for file uploading/downloading).
var DB = mongoose.connection;
var gfs = Grid(DB.db, mongoose.mongo);
var multipartMiddleware = multipart();

// Database collections.
var Users = mongoose.model('Users', USER_SCHEMA);
var Profiles = mongoose.model('Profiles', PROFILE_SCHEMA);
var Questions = mongoose.model('Questions', QUESTIONS_SCHEMA);
var Answers = mongoose.model('Answers', ANSWERS_SCHEMA);

//owasp password strength configuration.
owasp.config({
    allowPassphrases: false,
	maxLength: 128,
	minLength: 8,
	minPhraseLength: 20,
	minOptionalTestsToPass: 4,
});

// Post a question and store in database.
router.get('/newQuestion', function(req, res){
    var datetime = req.query.date;
    var UserName= req.session.username;
	console.log(UserName);
    if(UserName==undefined) {
		UserName="Anonymous";
	}
	var question = new Questions( {
	    Course: req.query.c, 
		Question: req.query.nq,
		Poster: UserName, 
		DateTime: datetime
	});
	question.save(function(err) {
		if (err) {
			res.status(500).send(err);
		    console.log(err);
			return;
		}
	});
	res.redirect('/question-posted');
});

// Post a new answer and store in database.
router.get('/newAnswer', function(req, res) {
    var newAnswer = req.query.nw;
    var datetime = req.query.date;
    var UserName= req.session.username;
    if(UserName==undefined) {
		UserName="Anonymous";
	}
	var answer = new Answers( {
	    Course: req.query.c, 
		Question: req.query.q,
		Poster: UserName, 
		DateTime: datetime,
		Answer: newAnswer
	});
	
	answer.save(function(err) {
		if (err) {
			res.status(500).send(err);
		    console.log(err);
			return;
		}
	});
	res.redirect('/answer-posted');
});

//Registers a new user
router.get('/register',function(req, res) {
	//Attempt to find the given user
    Users.find( {"UserName": req.query.uid}, function(err, username) {
	    if (err) {
		    res.status(500).send(err);
			console.log(err);
			return;
		}
		//Check if password is strong.
	    var password_check = owasp.test(req.query.pid);
	    if (password_check.strong == false) {
		    res.send("Invalid password");
        }
	    if (username == 0 && password_check.strong == true) {
			//Create a new user in the database
		    var user = new Users({
                FirstName: req.query.fnid,
                LastName: req.query.lnid,
                UserName: req.query.uid,
                Password: req.query.pid,
                Email: req.query.eid
            });
			
			//Create a new profile for the user, fill in with default values
			var profile = new Profiles({
				UserName: req.query.uid,
				School: "Unknown",
				Year: "1",
				Gender: "Other",
				Program: "Unknown",
				Picture: "/images/square.png"
			});
            user.save(function(err) {
		        if (err) {
			        res.status(500).send(err);
				    console.log(err);
				    return;
			    }
		    });
			profile.save(function(err) {
		        if (err) {
			        res.status(500).send(err);
				    console.log(err);
				    return;
			    }
		    });
			//Use a cookie to store the user name
			req.session.username = req.query.uid;
	        res.redirect('/../welcome');
		} 
		if (username != 0) {
		    res.send("Invalid user name");
		}
	});

});

router.post('/upload-file', multipartMiddleware, function(req, res, next) {
    var tempfile = req.files.filename.path;
	var originame = req.files.filename.name;
	//TODO: Enter the correct course name.
	var writestream = gfs.createWriteStream({ metadata: req.query.c, filename: originame });
	fs.createReadStream(tempfile)
	    .on('end', function() {
	        res.send('OK');
	    })
		.on('error', function() {
		    res.send('ERR');
		}) 
		.pipe(writestream);
	res.redirect('/thank-you?s='+req.query.c);
});

//Update new profile information sent by user
router.post('/editprofile', function(req, res) {
	//Read in request
	console.log(JSON.stringify(req.body));
	
	//Find the user's profile information
	Users.findOne({UserName: req.session.username},
					function(err, user){
		Profiles.findOne({UserName: req.session.username}, 
							function(err, profile){
			if (err) {
				res.status(500).send(err);
				console.log(err);
				return;
			}
			
			//Update the information if it is different that in database
			if (req.body.inputFirst != 0){
				//Followed documents for safer execution on write
				user.update({FirstName: req.body.inputFirst}).update();
				user.update({FirstName: req.body.inputFirst}).exec();
			}
			if (req.body.inputLast != 0){
				//Followed documents for safer execution on write
				user.update({LastName: req.body.inputLast}).update();
				user.update({LastName: req.body.inputLast}).exec();
			}
			if (req.body.inputSchool != 0){
				//Followed documents for safer execution on write
				profile.update({School: req.body.inputSchool}).update();
				profile.update({School: req.body.inputSchool}).exec();
			}
			if (req.body.inputProgram != 0){
				//Followed documents for safer execution on write
				profile.update({Program: req.body.inputProgram}).update();
				profile.update({Program: req.body.inputProgram}).exec();
			}
			if (req.body.inputEmail != 0){
				//Followed documents for safer execution on write
				user.update({Email: req.body.inputEmail}).update();
				user.update({Email: req.body.inputEmail}).exec();
			}
			if (req.body.inputYear != profile.Year){
				//Followed documents for safer execution on write
				profile.update({Year: req.body.inputYear}).update();
				profile.update({Year: req.body.inputYear}).exec();
			}
			if (req.body.inputGender != profile.Gender){
				//Followed documents for safer execution on write
				profile.update({Gender: req.body.inputGender}).update();
				profile.update({Gender: req.body.inputGender}).exec();
			}
		});
	});
	res.set({
		'method': 'GET'
	});
	
	//Return to edit profile again to see updates
    res.redirect('/query/editprofile');
});

//Save path to the user's profile picture
router.post('/defaultprofile', function(req, res) {
	console.log(JSON.stringify(req.body));
	
	//Retrieve the profile from database
	Profiles.findOne({UserName: req.session.username}, 
							function(err, profile){
			if (err) {
				res.status(500).send(err);
				console.log(err);
				return;
			}
			if (req.body.path != 0){
				//Followed documents for safer execution on write
				profile.update({Picture: req.body.path}).update();
				profile.update({Picture: req.body.path}).exec();
				res.send(200);
			}
	});
});

//Feature not yet available
//Allows users to upload custom profile picture
router.post('/customprofile', multipartMiddleware, function(req, res, next) {
	
    var tempfile = req.files.filename.path;
	var originame = req.files.filename.name;
	//TODO: Enter the correct course name.
	//var writestream = gfs.createWriteStream({ metadata: req.cookies.username, filename: originame });
	
	//console.log(req.files);
	//console.log(tempfile);
	//console.log(originame);
	
	gfs.files.find({ "metadata": req.cookies.username})
		.toArray(function(err, files){
			console.log(files);
	});
	
	/*
	fs.createReadStream(tempfile)
	    .on('end', function() {
	        res.send('OK');
	    })
		.on('error', function() {
		    res.send('ERR');
		}) 
		.pipe(writestream);
	res.render('editprofile');
	*/
	res.send(200);
});

module.exports = router;
