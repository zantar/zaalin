var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var fs = require('fs');
var Grid = require('gridfs-stream');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

var DB = mongoose.connection;
var gfs = Grid(DB.db, mongoose.mongo);

// Database collections.
var Users = mongoose.model('Users', USER_SCHEMA);
var Questions = mongoose.model('Questions', QUESTIONS_SCHEMA);
var Answers = mongoose.model('Answers', ANSWERS_SCHEMA);

router.get('/',function(req, res) {
	res.sendfile('public/StartupWindow.html');
});

router.get('/about-us', function(req, res) {
	res.sendfile('public/about-us.html');
});

router.get('/programs', function(req, res) {
	res.render('programs', {clickFunction: req.param('f')});
});

router.get('/CSC', function(req, res) {
	res.render('CSC', {clickFunction: req.param('f')});
});

router.get('/courseDescription', function(req, res) {
	res.render('courseDescription', {code: req.param('c')});
});

//Guest enterance to the site
//Will have session for guest for 3 days
router.get('/guest', function(req, res) {
	req.session.cookie.maxAge = 3 * 24 * 60 * 60;
	req.session.username = 'undefined';
	res.redirect('/welcome');
});

//Welcome page for site
router.get('/welcome', function(req, res) {
	res.render('main');
});

//Logout the user
//Currently similar to logging in as anon guest
router.get('/logout', function(req, res) {
	res.redirect('/guest');
});

//Load navbar
router.get('/navbar', function(req, res) {
	Users.find( {"UserName": req.session.username}, function(err, user) {
	    if (err) {
		    res.status(500).send(err);
			console.log(err);
			return;
		}
		// Username and password combo not found.
		if (user == 0) {
		    res.render('navbar', {
				firstname: "Guest",
				guest: "",
				user: "hidden"
			});
		}
		else{
			res.render('navbar', { 
				firstname: user[0].FirstName,
				guest:"hidden",
				user: ""
			});
		}
	});
});

router.get('/Forum', function(req,res) {
	var selected = req.query.s;
	if(selected == []) {
		res.send("Please select a course to view the forum.");
	}
	else {
		Questions.find({"Course" : selected}, function (err, question) {
			res.render('Forum', {"ForumArray": question, "course": selected});
		});
	}
});

router.get('/Answers', function(req, res) {
	var selected = req.query.s;
	if(selected==[]) {
		res.send("An error has occurred, please refresh page");
	}
	else {
		Answers.find({"Course" : selected, "Question": req.query.q}, 
		function (err, answers) {
			res.render('Answers', {"AnswerArray": answers, "question": req.query.q, "course": selected});
		});
	}
});

router.get('/past-items', function(req, res) {
	var selected = req.query.s;
	if(selected==[]) {
		res.send("Please select a course to view past items.");
	}
	else {
		gfs.files.find({ "metadata": selected})
		.toArray(function (err, files) {
		    res.render('past-items', {"pastitems": files, "course": selected});
		});
	}
});

// Page is used to refresh past items.
router.get('/thank-you', function(req, res) {
	var selected = req.query.s;
	if(selected==[]) {
		res.send("Please select a course to view past items.");
	}
	else {
		gfs.files.find({ "metadata": selected})
		.toArray(function (err, files) {
		    res.render('thank-you', {"course": selected});
		});
	}
});

router.get('/question-posted', function(req, res) {
    res.render('question-posted');
});

router.get('/answer-posted', function(req, res) {
    res.render('answer-posted');
});

module.exports = router;
