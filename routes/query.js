var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Grid = require('gridfs-stream');
var multipart = require('connect-multiparty');
var fs = require('fs');

var DB = mongoose.connection;
var gfs = Grid(DB.db, mongoose.mongo);
var multipartMiddleware = multipart();

// Database collections.
var Users = mongoose.model('Users', USER_SCHEMA);
var questions = mongoose.model('questions', QUESTIONS_SCHEMA);
var Profiles = mongoose.model('Profiles', PROFILE_SCHEMA);
var Programs = mongoose.model('Programs', PROGRAM_SCHEMA);
var Courses = mongoose.model('Courses', COURSE_SCHEMA);

//Get a list of programs
router.get('/programs', function(req, res) {
	res.contentType('json');

    Programs.find({},                 
    			function(err, programs) {
				    if (err) {
					    res.status(500).send(err);
						console.log(err);
						return;
					} else if (programs == 0) {
					    res.send("invalid");
					} else {
				    	res.send(programs);
					}
				}
	);
});

//Get a list of courses
router.get('/courses', function(req, res) {	
	res.contentType('json');
	
	Courses.find({"ProgramCode": req.param('q')},	
            function(err, courses) {
			    if (err) {
				    res.status(500).send(err);
					console.log(err);
					return;
				} else if (courses == 0) {
				    res.send("invalid");
				} else {
					res.send(courses);
				}
			}
	);
});

//Get a courses information
router.get('/course', function(req, res) {
	res.contentType('json');

	Courses.find({"Code": req.param('q')},	
                function(err, course) {
				    if (err) {
					    res.status(500).send(err);
						console.log(err);
						return;
					} else if (course == 0) {
					    res.send("invalid");
					} else {
				    	res.send(course);
				    }
				}
	);
});

//User login
router.get('/login', function(req, res) {	
	//Check if user name exists
    Users.find( {"UserName": req.query.user, "Password": req.query.pass}, 
				function(err, user) {
	    if (err) {
		    res.status(500).send(err);
			console.log(err);
			return;
		}
		// User name and password combo not found.
		if (user == 0) {
		    res.send("Invalid");
		}
		else{

			//Add to session user name
			//Change maxAge to null, should last 2 weeks in database
			req.session.username = req.query.user;
			req.session.cookie.maxAge = null;
			res.redirect('/welcome');
		}
	});
});

router.get('/viewAnswer', function(req, res) {
	if(req.query.s==[]) {
		req.send("Invalid");
	}
	else {
	    res.redirect('/Answers?q=' + req.query.q + "&s=" + req.query.s);
	}
});

//Display user's profile
router.get('/profile', function(req, res) {
	//Make sure the users don't enter this route without logging in
	if(req.session.username==undefined) {
		res.send(404);
	}

	//Retrieve user's information
	Users.findOne({"UserName": req.session.username}, function(err,user){
		if (err) {
				res.status(500).send(err);
				console.log(err);
				return;
		}
		//Retrieve the user's profile information
		Profiles.findOne({"UserName": req.session.username}, function(err,profile){
			if (err) {
				res.status(500).send(err);
				console.log(err);
				return;
			}
			// Username and password combo not found.
			if (user == 0) {
				res.send("Invalid");
			}
			
			//Check for what gender the user listed
			var gender;
			if (profile.Gender == "Male"){
				gender = "Male";
			}
			else if (profile.Gender == "Female"){
				gender = "Female";
			}
			else{
				gender = "Unknown";
			}
			
			//Render profile page will all information from database
			res.render('profile', {
				username: user.UserName,
				firstname: user.FirstName,
				lastname: user.LastName,
				email: user.Email,
				program: profile.Program,
				school: profile.School,
				year: profile.Year,
				gender: gender,
				picture: profile.Picture
			});
		});
	});
});

//Display the edit profile page
router.get('/editprofile', function(req, res) {
	//Make sure the users don't enter this route without logging in
	if(req.session.username==undefined) {
		res.send(404);
	}

	//Retrieve user's information
	Users.findOne({"UserName": req.session.username}, function(err,user){
		if (err) {
				res.status(500).send(err);
				console.log(err);
				return;
		}
		// Retrieve user's profile information
		Profiles.findOne({"UserName": req.session.username}, function(err,profile){
			if (err) {
				res.status(500).send(err);
				console.log(err);
				return;
			}
			// Username and password combo not found.
			if (user == 0) {
				res.send("Invalid");
			}
			var male, female, other;
			if (profile.Gender == "Male"){
				male = "checked"
				female = "";
				other = "";
			}
			else if (profile.Gender == "Female"){
				male = ""
				female = "checked";
				other = "";
			}
			else{
				male = ""
				female = "";
				other = "checked";
			}
			res.render('editprofile', {
				username: user.UserName,
				firstname: user.FirstName,
				lastname: user.LastName,
				email: user.Email,
				program: profile.Program,
				school: profile.School,
				year: profile.Year,
				male: male,
				female: female,
				other: other,
				picture: profile.Picture
			});
		});
	});
});

router.get('/download/:file', function (req, res) {
    var readstream = gfs.createReadStream({ filename: req.param('file') })
    readstream.pipe(res);
});

module.exports = router;