import re;
import os;

regGood = re.compile(r"<a name=\"(\D{3}\d{3}\D\d)" +
                     "(.*?)&nbsp;&nbsp;&nbsp;&nbsp;(.*?)</span><p>(.*?)</p>");

regFin = re.compile(r"<a name=\"(\D{3}\d{3})" +
                    "(.*?)&nbsp;&nbsp;&nbsp;&nbsp;(.*?)</span><p>(.*?)</p>" +
                    "(.*?(Prerequisite:(.*?))|.*?)(?=Corequisite:|Exclusion:|Distribution Requirement Status:|Breadth Requirement:)" +
                    "(.*?(Corequisite:(.*?))|.*?)(?=Exclusion:|Distribution Requirement Status:|Breadth Requirement:)" +
                    "(.*?(Exclusion:(.*?))|.*?)(?=Distribution Requirement Status:|Breadth Requirement:)" +
                    "(.*?(Distribution Requirement Status:(.*?))|.*?)(?=Breadth Requirement:)" +
                    "(.*?(Breadth Requirement:(.*))|.*?)");

formattedProg = open("json/programs/prog.json", "w");

for i in os.listdir("htm"):
    formattedProg.write("{\"Code\": \"" + i[4:7] + "\",");
    origin = open("htm/" + i);
    formatted = open("json/" + i[4:7] + ".json", "w");
    print(i);
    c1 = 0;
    c2 = 0;
    regBad = re.compile(r"<br>(.*)");
    for line in origin:
        matched = regGood.match(line);
        if matched:
            c1 += 1;
            cur = line;
            
            line = origin.readline();
            while not regBad.match(line) and line:
                cur = cur.strip() + ' ' + line.strip();
                line = origin.readline();
            matched = regFin.match(cur);
            if matched:
                c2 += 1;
                code = re.sub("\"", "\\\"", matched.group(1));
                name = re.sub("\"", "\\\"", matched.group(3));
                group = re.sub("\"", "\\\"", matched.group(4));
                vict = ("{\"ProgramCode\": \"" + i[4:7] + "\"," +
                                "\"Code\": \"" + code + "\"," +
                                "\"Name\": \"" + name + "\"," +
                                "\"Description\": \"" + group + "\",");
                group = matched.group(7);
                if(group):
                    group = re.sub("\"", "\\\"", group);
                    vict += ("\"Prerequisite\": \"" + group.strip() + "\",");
                else: vict += ("\"Prerequisite\": \"none\",");
                group = matched.group(10);
                if(group):
                    group = re.sub("\"", "\\\"", group);
                    vict += ("\"Corequisite\": \"" + group.strip() + "\",");
                else: vict += ("\"Corequisite\": \"none\",");
                group = matched.group(13);
                if(group):
                    group = re.sub("\"", "\\\"", group);
                    vict += ("\"Exclusion\": \"" + group.strip() + "\",");
                else: vict += ("\"Exclusion\": \"none\",");
                group = matched.group(19);
                if(group):
                    group = re.sub("\"", "\\\"", group);
                    vict += ("\"Breadth\": \"" + group.strip() + "\"}\n");
                else: vict += ("\"Breadth\": \"none\"}\n");
                vict = re.sub('<.*?>', '', vict);
                vict = re.sub('&nbsp;', ' ', vict);
                vict = re.sub('\t', ' ', vict);
                formatted.write(vict);
        elif line[0:4] == "<h1>":
            formattedProg.write("\"Name\": \"" + re.sub('<.*?>', '', line).strip() + "\"}\n");
    print(c1);
    print(c2);

formatted.close();
formattedProg.close();
origin.close();
