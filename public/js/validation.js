// Check for empty text boxes when signing up.
function emptySignUpField() {
    var x;
    x = document.getElementById("firstname").value;
    if (x == "") {
        alert("Your first name is required!");
        return false;
    };
	x = document.getElementById("lastname").value;
    if (x == "") {
        alert("Your last name is required!");
        return false;
    };
	x = document.getElementById("username").value;
    if (x == "") {
        alert("Your desired user name is required!");
        return false;
    };
	x = document.getElementById("password").value;
    if (x == "") {
        alert("Your desired password is required!");
        return false;
    };
	x = document.getElementById("email").value;
    if (x == "") {
        alert("Your e-mail address is required!");
        return false;
    };
}

// Check for empty text boxes when signing in.
function emptySignInField() {
    var y;
	y = document.getElementById("usern").value;
    if (y == "") {
        alert("Please input your user name.");
        return false;
    };
	y = document.getElementById("passw").value;
    if (y == "") {
        alert("Please input your password.");
        return false;
    };
}

// Log in validation.
$(document).ready(function() {                    
	$('#signin').on('click', function(e) {
	    e.preventDefault();
		$.ajax({
		    url: "query/login?user=" + $('#usern').val() + "&pass=" + $('#passw').val(),
			type: 'GET',
			success: function(res){
			    if (res == "Invalid" && $('#usern').val() != "" && $('#passw').val() != "") {
		            alert("Invalid user name or password.");
				} 
				if (res != "Invalid" && $('#usern').val() != "" && $('#passw').val() != "") {
					 window.location.href = "/welcome";
				}
	        }
		}); 
	});
});

// Sign up validation.
$(document).ready(function() {                    
	$('#signup').on('click', function(e) {
	    e.preventDefault();
		$.ajax({
		    url: "upload/register?fnid=" + $('#firstname').val() + "&lnid=" + $('#lastname').val()
			+ "&uid=" + $('#username').val() + "&pid=" + $('#password').val() + "&eid=" 
			+ $('#email').val(),
			type: 'GET',
			success: function(res){
			    if (res == "Invalid user name" && $('#firstname').val() != "" && $('#lastname').val() != ""
				    && $('#username').val() != "" && $('#password').val() != "" 
					&& $('#email').val() != "") {
		            alert("User name has already been taken.");
				}
                if (res == "Invalid password" && $('#firstname').val() != "" && $('#lastname').val() != ""
				    && $('#username').val() != "" && $('#password').val() != "" 
					&& $('#email').val() != "") {
		            alert("Your chosen password cannot be accepted because it is weak. Your password must:" + 
					     "\n - be at least 8 characters long" +
						 "\n - contain at least one upper case letter" +
						 "\n - contain at least one number" +
						 "\n - contain at least one special character");
				} 				
				if (res != "Invalid user name" && res != "Invalid password" 
				    && $('#firstname').val() != "" && $('#lastname').val() != ""
				    && $('#username').val() != "" && $('#password').val() != "" 
					&& $('#email').val() != "") {
					 alert("Successful registration.  Welcome to Course Square!");
					 window.location.href = "/welcome";
				}
	        }
		}); 
	});
});

// Check if a file to upload has been selected.
function emptyUploadForm() {
    var z;
	z = document.getElementById("upload-form").value;
    if (z == "") {
       alert("Please select a file to upload.");
       return false;
    };
}
