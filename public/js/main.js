function activeTab(x) {
	document.getElementById(x.id).style.background = "#5bc0de";
	document.getElementById(x.id).style.fontWeight = "bolder";
	document.getElementById(x.id).style.color="white";
	
	tabs = document.getElementsByClassName("tabs");
	
	for (var i = 0; i < tabs.length; i++) {
		if (tabs[i].id != x.id){
			tabs[i].style.background="#f8f8f8";
			tabs[i].style.fontWeight="normal";
			tabs[i].style.color="#545454";
		}
	}
		
}

function onload(){
	tabs = document.getElementsByClassName("tabs");
	
	maxwidth = 0;
	for (var i = 0; i < tabs.length; i++) {
		if (tabs[i].offsetWidth > maxwidth)
			maxwidth = tabs[i].offsetWidth;
	}
		
	for (var i = 0; i < tabs.length; i++) {
		tabs[i].style.width = (maxwidth + 20) + "px";
		if (tabs[i].id == "program") {
			tabs[i].style.background="#5bc0de";
			tabs[i].style.fontWeight = "bolder";
			tabs[i].style.color="white";
		}
		else {
			tabs[i].style.background="#f8f8f8";
			tabs[i].style.color="#545454";
		}
	}

}