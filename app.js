var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');
var mongoose = require('mongoose');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

//Database Connection
mongoose.connect('mongodb://localhost:12345/CSDB');
var DB = mongoose.connection;
DB.on('error', console.error.bind(console, 'connection error:'));
DB.once('open', function callback () {
    console.log('Connected to MongoDB');
});

// Validation function (prevents empty fields).
function validatePresenceOf(value) {
    return value && value.length;
}
  
//Database Schemas
USER_SCHEMA = mongoose.Schema({
    FirstName: { type: String, validate: [validatePresenceOf, 'First name is required!'] },
    LastName: { type: String, validate: [validatePresenceOf, 'Last name is required!'] },
    UserName: { type: String, validate: [validatePresenceOf, 'Username is required!'] },
    Password: { type: String, validate: [validatePresenceOf, 'Password is required!'] },
    Email: { type: String, validate: [validatePresenceOf, 'Email is required!'] }
});

QUESTIONS_SCHEMA = mongoose.Schema({
    Course: { type: String },
	Question: { type: String },
	Poster: { type: String },
    DateTime: { type: String }
});

ANSWERS_SCHEMA = mongoose.Schema({
    Course: { type: String },
	Question: { type: String },
	Poster: { type: String },
    DateTime: { type: String },
	Answer: { type: String }
});

PROFILE_SCHEMA = mongoose.Schema({
	Picture: {type: String },
    School: { type: String },
	Year: { type: Number },
	UserName: { type: String },
	Gender: { type: String },
	Program: { type: String },
});

PROGRAM_SCHEMA = mongoose.Schema({
    Code: { type: String, validate: [validatePresenceOf, 'Code is required!'] },
    Name: { type: String, validate: [validatePresenceOf, 'Name is required!'] }
});

//COURSE_SCHEMA Program corresponds to some PROGRAM_SCHEMA Code 
COURSE_SCHEMA = mongoose.Schema({
    ProgramCode: { type: String, validate: [validatePresenceOf, 'ProgramCode is required!'] },
    Code: { type: String, validate: [validatePresenceOf, 'Code is required!'] },
    Name: { type: String, validate: [validatePresenceOf, 'Name is required!'] },
    Description: { type: String, validate: [validatePresenceOf, 'Description is required!'] },
    Corequisite: { type: String },
    Prerequisite: { type: String },
    Exclusion: { type: String },
    Breadth: { type: String, validate: [validatePresenceOf, 'Breadth is required!'] }
});

var routes = require('./routes/index');
var dbQuery = require('./routes/query');
var dbUpload = require('./routes/upload');

var app = express();

//Allow compression for all files being sent
app.use(compression({
	filter: function (req, res) {
		return true;
	}
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hjs');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(session({
	//Change default session settings to match our mongo server
	name: 'user',
	secret: 'zaalin',
	store: new MongoStore({
		db: 'CSDB',
		host: 'localhost',
		port: 12345
	}),
	resave: 'true',
	saveUninitialized: 'true'
}));
app.use(express.static(path.join(__dirname, 'public')));

//Routes db query requestes 
app.use('/query', dbQuery);

//Routes db upload data requests
app.use('/upload', dbUpload);

//Routes other requestes (this is for static pages)
app.use('/', routes);

/// catch 404 and forward to error handler
app.use(function(req, res, next) { 
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
